from django.urls import path

from basic_app import views

urlpatterns = [
    path('teachers/', views.TeacherList.as_view(), name='teachers'),
    path('teachers/<int:pk>', views.TeacherDetail.as_view(), name='teacher_detail'),
    path('parties/', views.PartyList.as_view(), name='party'),
    path('parties/<int:pk>', views.PartyDetail.as_view(), name='party_detail'),
    path('news/', views.NewsList.as_view(), name='news'),
    path('news/<int:pk>', views.NewsDetail.as_view(), name='news_detail'),
    path('school/', views.SchoolListCreateView.as_view(), name='school'),
    path('corusel/', views.CoruselCreateList.as_view(), name='corusel'),
    path('who-are-we/', views.WhoAreWeCreateList.as_view(), name='who_are_we'),
    path('our-mission/', views.OurMissionCreateList.as_view(), name='our_mission'),
    path('our-school/', views.OurSchoolCreateList.as_view(), name='our_school'),
    path('news-banner/', views.NewsBannerListView.as_view(), name='news_banner'),
    path('logo/', views.LogoModelListView.as_view(), name='logo_model'),
    path('life-of-school/', views.LifeOfSchoolListView.as_view(), name='life_of_school'),
    path('main-description/', views.MainDescriptionListView.as_view(), name='main_description'),
    path('acceptance/', views.AcceptanceListView.as_view(), name='acceptance'),
    path('reception/', views.ReceptionListView.as_view(), name='reception'),
    path('result-of-olympics/', views.ResultOfOlympicsListView.as_view(), name='result_of_olympics'),
    path('result-olympic/', views.ResultOlympicListView.as_view(), name='result_olympics'),
    path('school-uniform/', views.SchoolUniformListView.as_view(), name='school_uniform'),
    path('photo-gallery/', views.PhotoGalleryListView.as_view(), name='photo_gallery'),
    path('education-instruments/', views.EducationInstrumentListView.as_view(), name='education'),
]
