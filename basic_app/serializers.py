from rest_framework import serializers
import requests
from basic_app import models

BOT_TOKEN = '2085784948:AAGSAd9SGVcoWjAlwJp_CGx0Cag0x47yjeY'


class TeacherImageSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.TeacherImages
        fields = ('id', 'image')


class TeacherSerializers(serializers.ModelSerializer):
    imgs = TeacherImageSerializers(many=True)

    class Meta:
        model = models.Teachers
        fields = '__all__'


class PartySerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Party
        fields = '__all__'


class NewsImagesSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.NewsImages
        fields = '__all__'


class NewsSerializers(serializers.ModelSerializer):
    images = NewsImagesSerializers(many=True)

    class Meta:
        model = models.News
        fields = '__all__'


class SchoolSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.School
        fields = '__all__'

    def create(self, validated_data):
        text = f'''Tel: {validated_data['phone']}\nFIO: {validated_data['name']}\nSinfi: {validated_data['y_class']}\nSababi: {validated_data['cause']}'''
        ids = models.TelegramAdminID.objects.all().values_list('chat_id', flat=True)
        for i in ids:
            send_request(i, text)
        return super(SchoolSerializers, self).create(validated_data)


def send_request(id, text):
    requests.post('https://api.telegram.org/bot{token}/sendMessage?chat_id={id}&text={text}'.format(token=BOT_TOKEN,
                                                                                                    id=id,
                                                                                                    text=text))


class FoodSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Food
        fields = '__all__'


class CoruselSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Corusel
        fields = '__all__'


class WhoAreWeSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.WhoAreWe
        fields = '__all__'


class OurMissionSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.OurMission
        fields = '__all__'


class OurSchoolSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.OurScholl
        fields = '__all__'


class NewsBannerSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.NewsBanner
        fields = '__all__'


class LogoModelSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.LogoModel
        fields = '__all__'


class LifeOfSchoolSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.LifeOfSchool
        fields = '__all__'


class MainDescriptionSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.MainDescription
        fields = '__all__'


class AcceptanceSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.Acceptance
        fields = '__all__'


class ReceptionSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.ReceptionModel
        fields = '__all__'


class SchoolUniformSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.SchoolUniform
        fields = '__all__'


class ResultOfOlymicsSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.ResultOfOlympics
        fields = '__all__'


class TextImageDesAbstrackSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.TextImageDesAbstrack
        fields = '__all__'


class ResultOlympicSerializers(serializers.ModelSerializer):
    class Meta:
        model = models.ResultOlympic
        fields = '__all__'
