from django.db import models


# Create your models here.


class Teachers(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    video = models.FileField(upload_to='videos/', default=None)
    subject = models.CharField(max_length=50)
    subject_en = models.CharField(max_length=50, default=None, null=True)
    subject_ru = models.CharField(max_length=50, default=None, null=True)
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.first_name


class TeacherImages(models.Model):
    image = models.ImageField(upload_to='images/')
    teacher = models.ForeignKey(Teachers, related_name='imgs', on_delete=models.CASCADE)

    def __str__(self):
        return str(self.teacher)


class Party(models.Model):
    name = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100, default=None, null=True)
    name_ru = models.CharField(max_length=100, default=None, null=True)
    image = models.ImageField(upload_to='images')
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)
    date = models.DateTimeField()

    def __str__(self):
        return self.name


class News(models.Model):
    name = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100, default=None, null=True)
    name_ru = models.CharField(max_length=100, default=None, null=True)
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)
    date = models.DateTimeField()

    def __str__(self):
        return self.name


class NewsImages(models.Model):
    news = models.ForeignKey(News, related_name='images', on_delete=models.CASCADE)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return str(self.news)


class School(models.Model):
    phone = models.CharField(max_length=20)
    name = models.CharField(max_length=100)
    name_en = models.CharField(max_length=100, default=None, null=True)
    name_ru = models.CharField(max_length=100, default=None, null=True)
    y_class = models.CharField(max_length=20)
    cause = models.TextField()

    def __str__(self):
        return self.name


class Food(models.Model):
    category = models.CharField(max_length=20, choices=[
        ("Breakfast", "Breakfast"),
        ("Dinner", "Dinner"),
        ("Lunch", "Lunch"),
    ])
    name = models.CharField(max_length=50)
    name_en = models.CharField(max_length=50, default=None, null=True)
    name_ru = models.CharField(max_length=50, default=None, null=True)
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)
    image = models.ImageField(upload_to='images/')


class Corusel(models.Model):
    title = models.CharField(max_length=100)
    title_en = models.CharField(max_length=100, default='')
    title_ru = models.CharField(max_length=100, default='')
    image = models.ImageField(upload_to='images/')
    description = models.TextField()
    description_en = models.TextField(default='')
    description_ru = models.TextField(default='')

    def __str__(self):
        return self.title


class WhoAreWe(models.Model):
    title = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200)
    title_ru = models.CharField(max_length=200)
    video = models.FileField(upload_to='videos/')
    description = models.TextField()
    description_en = models.TextField()
    description_ru = models.TextField()

    def __str__(self):
        return self.title


class OurMission(models.Model):
    name = models.CharField(max_length=200)
    name_en = models.CharField(max_length=200, default=None, null=True)
    name_ru = models.CharField(max_length=200, default=None, null=True)
    title = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200, default=None, null=True)
    title_ru = models.CharField(max_length=200, default=None, null=True)
    image = models.FileField(upload_to='images/')
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.name


class OurScholl(models.Model):
    img = models.ImageField(upload_to='images/')
    title = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200, default=None, null=True)
    title_ru = models.CharField(max_length=200, default=None, null=True)
    text = models.TextField()
    text_en = models.TextField(default=None, null=True)
    text_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.title


class NewsBanner(models.Model):
    img = models.ImageField(upload_to='images/')
    title = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200, default=None, null=True)
    title_ru = models.CharField(max_length=200, default=None, null=True)
    text = models.TextField()
    text_en = models.TextField(default=None, null=True)
    text_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.title


class LogoModel(models.Model):
    img = models.ImageField(upload_to='images/')
    phone = models.CharField(max_length=20)


class ReceptionModel(models.Model):
    img = models.ImageField(upload_to='images/')
    title = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200, default=None, null=True)
    title_ru = models.CharField(max_length=200, default=None, null=True)
    text = models.TextField()
    text_en = models.TextField(default=None, null=True)
    text_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.title


class LifeOfSchool(models.Model):
    img = models.ImageField(upload_to='images/')
    text = models.TextField()
    text_en = models.TextField(default=None, null=True)
    text_ru = models.TextField(default=None, null=True)
    description = models.TextField(default=None, null=True)
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.text


class MainDescription(models.Model):
    title = models.CharField(max_length=100)
    title_en = models.CharField(max_length=100, default=None, null=True)
    title_ru = models.CharField(max_length=100, default=None, null=True)
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)
    img = models.ImageField(upload_to='images/', default=None, null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Qabul Bannerlar'
        verbose_name = 'Qabul Banner'


class Acceptance(models.Model):
    text = models.TextField()
    text_en = models.TextField(default=None, null=True)
    text_ru = models.TextField(default=None, null=True)
    image = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.text


class SchoolUniform(models.Model):
    description = models.CharField(max_length=200)
    description_en = models.CharField(max_length=200, default=None, null=True)
    description_ru = models.CharField(max_length=200, default=None, null=True)
    left_title1 = models.CharField(max_length=50)
    left_title1_ru = models.CharField(max_length=50, default=None, null=True)
    left_title1_en = models.CharField(max_length=50, default=None, null=True)
    left_title2 = models.CharField(max_length=50)
    left_title2_en = models.CharField(max_length=50, default=None, null=True)
    left_title2_ru = models.CharField(max_length=50, default=None, null=True)
    left_title3 = models.CharField(max_length=50)
    left_title3_en = models.CharField(max_length=50, default=None, null=True)
    left_title3_ru = models.CharField(max_length=50, default=None, null=True)
    right_title1 = models.CharField(max_length=50)
    right_title1_en = models.CharField(max_length=50, default=None, null=True)
    right_title1_ru = models.CharField(max_length=50, default=None, null=True)
    right_title2 = models.CharField(max_length=50)
    right_title2_en = models.CharField(max_length=50, default=None, null=True)
    right_title2_ru = models.CharField(max_length=50, default=None, null=True)
    right_title3 = models.CharField(max_length=50)
    right_title3_en = models.CharField(max_length=50, default=None, null=True)
    right_title3_ru = models.CharField(max_length=50, default=None, null=True)
    image_uniform = models.ImageField(upload_to='images/')

    def __str__(self):
        return self.description


class ResultOfOlympics(models.Model):
    text = models.CharField(max_length=100)
    text_en = models.CharField(max_length=100, default='')
    text_ru = models.CharField(max_length=100, default='')
    image = models.ImageField(upload_to='images/')
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.text


class TelegramAdminID(models.Model):
    chat_id = models.PositiveSmallIntegerField()

    def __str__(self):
        return f'{self.chat_id}'


class TextImageDesAbstrack(models.Model):
    text = models.CharField(max_length=100)
    text_en = models.CharField(max_length=100, default='')
    text_ru = models.CharField(max_length=100, default='')
    image = models.ImageField(upload_to='images/')
    description = models.TextField()
    description_en = models.TextField(default=None, null=True)
    description_ru = models.TextField(default=None, null=True)

    def __str__(self):
        return self.text


class PhotoGallery(TextImageDesAbstrack):
    pass


class EducationInstruments(TextImageDesAbstrack):
    pass


class ResultOlympic(models.Model):
    number = models.CharField(max_length=20)
    title = models.CharField(max_length=200)
    title_ru = models.CharField(max_length=200)
    title_en = models.CharField(max_length=200)
