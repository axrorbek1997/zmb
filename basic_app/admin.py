from django.contrib import admin
from . import models


# Register your models here.
@admin.register(models.School)
class SchoolAdmin(admin.ModelAdmin):
    list_display = ('phone', 'name', 'y_class')


@admin.register(models.News)
class NewsAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')


admin.site.register(models.NewsImages)
admin.site.register(models.TeacherImages)
admin.site.register(models.Corusel)
admin.site.register(models.WhoAreWe)
admin.site.register(models.OurMission)
admin.site.register(models.OurScholl)
admin.site.register(models.NewsBanner)
admin.site.register(models.LogoModel)
admin.site.register(models.LifeOfSchool)
admin.site.register(models.MainDescription)
admin.site.register(models.Acceptance)
admin.site.register(models.ReceptionModel)
admin.site.register(models.SchoolUniform)
admin.site.register(models.TelegramAdminID)
admin.site.register(models.ResultOfOlympics)
admin.site.register(models.PhotoGallery)
admin.site.register(models.EducationInstruments)
admin.site.register(models.ResultOlympic)


@admin.register(models.Party)
class PartyAdmin(admin.ModelAdmin):
    list_display = ('name', 'date')


@admin.register(models.Teachers)
class TeachersAdmin(admin.ModelAdmin):
    list_display = ('first_name', 'last_name')
