from rest_framework.generics import ListAPIView, ListCreateAPIView, RetrieveAPIView

from basic_app import models, serializers


class TeacherList(ListAPIView):
    queryset = models.Teachers.objects.all()
    serializer_class = serializers.TeacherSerializers


class TeacherDetail(RetrieveAPIView):
    lookup_field = 'pk'
    queryset = models.Teachers.objects.all()
    serializer_class = serializers.TeacherSerializers


class PartyList(ListAPIView):
    queryset = models.Party.objects.all()
    serializer_class = serializers.PartySerializers


class PartyDetail(RetrieveAPIView):
    lookup_field = 'pk'
    queryset = models.Party.objects.all()
    serializer_class = serializers.PartySerializers


class NewsList(ListAPIView):
    queryset = models.News.objects.all()
    serializer_class = serializers.NewsSerializers


class NewsDetail(RetrieveAPIView):
    lookup_field = 'pk'
    queryset = models.News.objects.all()
    serializer_class = serializers.NewsSerializers


class SchoolListCreateView(ListCreateAPIView):
    queryset = models.School.objects.all()
    serializer_class = serializers.SchoolSerializers


class FoodList(ListAPIView):
    # authentication_classes = (JWTAuthentication, SessionAuthentication, BasicAuthentication)
    # permission_classes = (IsAdminUser,)
    queryset = models.Food.objects.all()
    serializer_class = serializers.FoodSerializers


class CoruselCreateList(ListAPIView):
    queryset = models.Corusel.objects.all()
    serializer_class = serializers.CoruselSerializers


class WhoAreWeCreateList(ListAPIView):
    queryset = models.WhoAreWe.objects.all()
    serializer_class = serializers.WhoAreWeSerializers


class OurMissionCreateList(ListAPIView):
    queryset = models.OurMission.objects.all()
    serializer_class = serializers.OurMissionSerializers


class OurSchoolCreateList(ListAPIView):
    queryset = models.OurScholl.objects.all()
    serializer_class = serializers.OurSchoolSerializers


class NewsBannerListView(ListAPIView):
    queryset = models.NewsBanner.objects.all()
    serializer_class = serializers.NewsBannerSerializers


class LogoModelListView(ListAPIView):
    queryset = models.LogoModel.objects.all()
    serializer_class = serializers.LogoModelSerializers


class LifeOfSchoolListView(ListAPIView):
    queryset = models.LifeOfSchool.objects.all()
    serializer_class = serializers.LifeOfSchoolSerializers


class MainDescriptionListView(ListAPIView):
    queryset = models.MainDescription.objects.all()
    serializer_class = serializers.MainDescriptionSerializers


class AcceptanceListView(ListAPIView):
    queryset = models.Acceptance.objects.all()
    serializer_class = serializers.AcceptanceSerializers


class ReceptionListView(ListAPIView):
    queryset = models.ReceptionModel.objects.all()
    serializer_class = serializers.ReceptionSerializers


class SchoolUniformListView(ListAPIView):
    queryset = models.SchoolUniform.objects.all()
    serializer_class = serializers.SchoolUniformSerializers


class ResultOfOlympicsListView(ListAPIView):
    queryset = models.ResultOfOlympics.objects.all()
    serializer_class = serializers.ResultOfOlymicsSerializers


class PhotoGalleryListView(ListAPIView):
    queryset = models.PhotoGallery.objects.all()
    serializer_class = serializers.TextImageDesAbstrackSerializers


class EducationInstrumentListView(ListAPIView):
    queryset = models.EducationInstruments.objects.all()
    serializer_class = serializers.TextImageDesAbstrackSerializers


class ResultOlympicListView(ListAPIView):
    queryset = models.ResultOlympic.objects.all()
    serializer_class = serializers.ResultOlympicSerializers
